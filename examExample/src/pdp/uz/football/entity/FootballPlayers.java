package pdp.uz.football.entity;

import java.io.Serializable;

public class FootballPlayers implements Serializable {
    private String playerName;
    private Integer id;

    public FootballPlayers(String playerName, Integer id) {
        this.playerName = playerName;
        this.id = id;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "FootballPlayers{" +
                "playerName='" + playerName + '\'' +
                ", id=" + id +
                '}';
    }
}
