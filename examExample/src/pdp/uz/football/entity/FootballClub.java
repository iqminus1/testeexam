package pdp.uz.football.entity;

import java.io.Serializable;
import java.time.LocalDate;

public class FootballClub implements Serializable {
    private String clubName;
    private Integer id;
    private String coach;
    private LocalDate writtenDate;
    private String path;
    private FootballPlayers footballPlayers;

    public FootballClub(String clubName, Integer id, String coach, LocalDate writtenDate, String path, FootballPlayers footballPlayers) {
        this.clubName = clubName;
        this.id = id;
        this.coach = coach;
        this.writtenDate = writtenDate;
        this.path = path;
        this.footballPlayers = footballPlayers;
    }

    public String getClubName() {
        return clubName;
    }

    public void setClubName(String clubName) {
        this.clubName = clubName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCoach() {
        return coach;
    }

    public void setCoach(String coach) {
        this.coach = coach;
    }

    public LocalDate getWrittenDate() {
        return writtenDate;
    }

    public void setWrittenDate(LocalDate writtenDate) {
        this.writtenDate = writtenDate;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public FootballPlayers getFootballPlayers() {
        return footballPlayers;
    }

    public void setFootballPlayers(FootballPlayers footballPlayers) {
        this.footballPlayers = footballPlayers;
    }

    @Override
    public String toString() {
        return "FootballClub{" +
                "clubName='" + clubName + '\'' +
                ", id=" + id +
                ", coach='" + coach + '\'' +
                ", writtenDate=" + writtenDate +
                ", path='" + path + '\'' +
                ", footballPlayers=" + footballPlayers +
                '}';
    }
}
