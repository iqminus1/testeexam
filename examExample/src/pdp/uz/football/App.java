package pdp.uz.football;

import pdp.uz.football.entity.FootballClub;
import pdp.uz.football.entity.FootballPlayers;
import pdp.uz.football.service.ClubServiceImpl;
import pdp.uz.football.service.PlayersServiceImpl;

import java.time.LocalDate;
import java.util.List;

public class App {
    private static ClubServiceImpl clubService = ClubServiceImpl.getInstance();
    private static PlayersServiceImpl playersService = PlayersServiceImpl.getInstance();
    public static void main(String[] args) {
        System.out.println(clubService.deserialize());
        System.out.println(clubService.getByClubID(4));
        System.out.println(clubService.delete(4));
        System.out.println(clubService.readByPath(6));
        addMethod();
        clubService.update(3,new FootballClub(
                "RMA ",
                null,
                "Ser " ,
                LocalDate.now(),
                "forReader/file.txt",
                playersService.get(7)));
        System.out.println(clubService.getByClubID(3));

//        marge();
//        System.out.println(clubService.getById(3));
//        System.out.println(playersService.read());
//        System.out.println(clubService.serialize());
//        System.out.println(clubService.getById(7));
    }
    private static void marge() {
        for (int i = 0; i < 400; i++) {
            clubService.add(new FootballClub(
                    "Real Madrid " + i,
                    null,
                    "Zidan " + i,
                    LocalDate.now(),
                    "forReader/file.txt",
                    playersService.get(i%10)));
        }
    }

    private static void addMethod() {
        for (int i = 0; i < 10; i++) {
            playersService.cread(new FootballPlayers("Player " + i, null));
        }
    }
}
