package pdp.uz.football.service;

import pdp.uz.football.entity.FootballClub;
import pdp.uz.football.entity.FootballPlayers;

import java.util.List;

public interface ClubService {
    List<FootballClub> getById(Integer playerId);
    FootballClub getByClubID(Integer clubId);
    FootballClub add(FootballClub club);
    FootballClub update(Integer clubId,FootballClub club);
    boolean delete(Integer id);
    String readByPath(Integer id);
    boolean serialize();
    boolean deserialize();

}
