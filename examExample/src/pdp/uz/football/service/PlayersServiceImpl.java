package pdp.uz.football.service;

import pdp.uz.football.entity.FootballPlayers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

public class PlayersServiceImpl implements PlayersService {
    private List<FootballPlayers> players = Collections.synchronizedList(new ArrayList<>());
    private static ReentrantLock lock = new ReentrantLock();

    private PlayersServiceImpl() {
    }

    private static PlayersServiceImpl instance;

    public static PlayersServiceImpl getInstance() {
        if (instance == null) {
            lock.lock();
            if (instance == null) {
                instance = new PlayersServiceImpl();
            }
            lock.unlock();
        }
        return instance;
    }

    @Override
    public FootballPlayers cread(FootballPlayers footballPlayers) {
        footballPlayers.setId(players.size() + 1);
        players.add(footballPlayers);
        return footballPlayers;
    }

    @Override
    public List<FootballPlayers> read() {
        return players;
    }

    @Override
    public FootballPlayers update(Integer id, FootballPlayers footballPlayers) {
        FootballPlayers player = players.stream()
                .filter(p -> p.getId().equals(id))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("updateda optional classiga null qiymat keb qoldi"));
        player.setId(id);
        player.setPlayerName(footballPlayers.getPlayerName());
        return player;
    }

    @Override
    public boolean delete(Integer id) {
        FootballPlayers player = players.stream()
                .filter(p -> p.getId().equals(id))
                .findFirst()
                .orElseThrow();
        players.stream()
                .filter(p -> p.getId() > id)
                .forEach(p-> p.setId(p.getId()-1));
        return players.remove(player);
    }
    public FootballPlayers get(Integer index){
        return players.get(index);
    }
}
