package pdp.uz.football.service;

import pdp.uz.football.entity.FootballPlayers;

import java.util.List;

public interface PlayersService {
    FootballPlayers cread(FootballPlayers footballPlayers);

    List<FootballPlayers> read();

    FootballPlayers update(Integer id, FootballPlayers footballPlayers);

    boolean delete(Integer id);
}
