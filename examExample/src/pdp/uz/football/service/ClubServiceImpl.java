package pdp.uz.football.service;

import pdp.uz.football.entity.FootballClub;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.*;
import java.util.stream.Collectors;

public class ClubServiceImpl implements ClubService {
    private static ReentrantLock lock = new ReentrantLock();

    private ClubServiceImpl() {
        try {
            log = Logger.getLogger("ClubService");
            FileHandler fileHandler = new FileHandler("logs/log.txt", true);
            fileHandler.setFormatter(new SimpleFormatter());
            log.addHandler(fileHandler);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static ClubServiceImpl instance;

    public static ClubServiceImpl getInstance() {
        if (instance == null) {
            lock.lock();
            if (instance == null) {
                instance = new ClubServiceImpl();
            }
            lock.unlock();
        }
        return instance;
    }

    private Logger log;

    private List<FootballClub> clubs = Collections.synchronizedList(new ArrayList<>());

    @Override
    public List<FootballClub> getById(Integer playerId) {
        log.log(Level.INFO, " id boyicha olinish bajarildi\n");
        List<FootballClub> collect = clubs.stream()
                .filter(club -> club.getFootballPlayers().getId().equals(playerId))
                .collect(Collectors.toList());
        return collect;
    }

    @Override
    public FootballClub getByClubID(Integer clubId) {
        log.log(Level.INFO, " id boyicha club olinish bajarildi\n");
        FootballClub footballClub = clubs.stream()
                .filter(club -> club.getId().equals(clubId))
                .findFirst()
                .orElseThrow();
        return footballClub;
    }

    @Override
    public FootballClub add(FootballClub club) {
        log.log(Level.INFO, " add method ishladi\n");
        club.setId(clubs.size() + 1);
        clubs.add(club);
        return club;
    }

    @Override
    public FootballClub update(Integer clubId, FootballClub club) {
        log.log(Level.INFO, " update method ishladi\n");
        FootballClub footballClub = clubs.stream()
                .filter(club1 -> club1.getId().equals(clubId))
                .findFirst()
                .orElseThrow();
        footballClub.setClubName(club.getClubName());
        footballClub.setFootballPlayers(club.getFootballPlayers());
        footballClub.setPath(club.getPath());
        footballClub.setCoach(club.getCoach());
        footballClub.setWrittenDate(club.getWrittenDate());

        return footballClub;
    }

    @Override
    public boolean delete(Integer id) {
        log.log(Level.INFO, " delete method ishladi\n");
        FootballClub footballClub = clubs.stream()
                .filter(club -> club.getId().equals(id))
                .findFirst()
                .orElseThrow();
        clubs.stream()
                .filter(club -> club.getId() > id)
                .forEach(club -> club.setId(club.getId()-1));
        return clubs.remove(footballClub);
    }

    @Override
    public String readByPath(Integer id) {
        try (InputStream input = new FileInputStream("forReader/file.txt")) {
            byte[] bytes = input.readAllBytes();
            log.log(Level.INFO, " readByPath methodi togri ishladi\n");
            return new String(bytes);
        } catch (IOException e) {
            log.log(Level.SEVERE, " readByPath methoda xatolikga tushdi\n");
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean serialize() {
        try (ObjectOutputStream output = new ObjectOutputStream(new FileOutputStream("serialize/objectList.txt"))) {
            output.writeObject(clubs);
            log.log(Level.INFO, " serialize methodi togri ishladi\n");
            return true;
        } catch (IOException e) {
            log.log(Level.SEVERE, " serialize methoda xatolikga tushdi\n");
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean deserialize() {
        try (ObjectInputStream input = new ObjectInputStream(new FileInputStream("serialize/objectList.txt"))) {
            List<FootballClub> clubs1 = (List<FootballClub>) input.readObject();
            if (Objects.nonNull(clubs1)) {
                clubs = clubs1;
                return true;
            }
            log.log(Level.INFO, " deserialize methodi togri ishladi\n");
        } catch (IOException e) {
            log.log(Level.SEVERE, " deserialize methoda xatolikga tushdi\n");
            throw new RuntimeException(e);
        } catch (ClassNotFoundException e) {
            log.log(Level.SEVERE, " deserialize methoda xatolikga tushdi\n");
            throw new RuntimeException(e);
        }
        return false;
    }
}
